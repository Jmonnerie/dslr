# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    parse.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/20 18:52:40 by jojomoon          #+#    #+#              #
#    Updated: 2020/05/08 16:45:10 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import os
from sample import strings
from sample.dataset import Dataset
from sample.exceptions import UsageError, CSVError


def dataSet(dataFile):
    # Take datas
    if not os.path.exists(dataFile):
        print(strings.parseFileNotFound % dataFile)
        raise UsageError()
    fd = open(dataFile, "r")
    lines = fd.readlines()
    fd.close()
    # Take data lenght according to the first line and init the array
    nbFeat = len(lines[0].split(","))
    datas = []
    # Check and split all lines
    for idx in range(len(lines)):
        values = lines[idx].split(",")
        values[-1] = values[-1].replace(
            "\n", ""
        )  # la fin de la ligne comporte un \n => on l'enleve
        if len(values) != nbFeat:
            print(strings.parseErrorLineHasNotGoodFeatNumber % (dataFile, idx + 1))
            raise CSVError()
        datas.append(values)
    # Initialise a dataset
    return Dataset(datas)
