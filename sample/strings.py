# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    strings.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/20 18:03:36 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/22 17:15:25 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

# Colors
red = "\033[31m"
green = "\033[32m"
yellow = "\033[33m"
blue = "\033[34m"
magenta = "\033[35m"
cian = "\033[36m"
white = "\033[37m"
normal = "\033[0m"

# Misc
default_data_file = "./data/dataset_train.csv"

# Dataset
both_data_send = "csv_file and array send to Dataset"
no_data_send = "no data send to Dataset"

# Describe
describe_warning_no_param = (
    yellow
    + 'Warning: No data file specified in parameter. "./data/dataset_train.csv" set by default'
    + normal
)
describe_usage = (
    "Usage:\n"
    "$ python3.7 ./sample/describe.py [dataFile]\n"
    f"dataFile = path to a dataset to describe. {default_data_file} by default."
)

# Parse
parseFileNotFound = f"{red}Error: %s : file not found.{normal}"
row_wrong_format = f"{red}Error: csv file %s line %d: row not well formated{normal}"
