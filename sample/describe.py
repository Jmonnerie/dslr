import sys
import strings
from dataset import Dataset


def describe(csv_file):
    ds = Dataset(csv_file=csv_file)
    return ds.describe()


if __name__ == "__main__":
    file = None
    if len(sys.argv) < 2:
        print(strings.describe_warning_no_param)
        file = strings.default_data_file
    describe(file or sys.argv[1])
