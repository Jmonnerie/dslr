# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    dataset.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/04 01:16:37 by jojomoon          #+#    #+#              #
#    Updated: 2020/05/08 18:34:07 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import csv
import strings


class Dataset(object):
    """ Dataset class
        Can be initialised with a csv_file_path OR an array
        If array is send and not keys, keys are extracted from first array row
        raises ValueError if both are send.
    """

    def __init__(self, array=None, keys=None, csv_file=None):
        if csv_file and array:
            raise ValueError(strings.both_data_send)
        if not csv_file and not array:
            raise ValueError(strings.no_data_send)
        if csv_file:
            self.load_csv(csv_file)
            return
        if keys is None:
            keys = array.pop(0)
        self.load_array(keys, array)

    def __str__(self):
        ret = (
            "dataset :\n" + f"- nbFeat = {self.nbFeat}\n" + f"- nbData = {self.nbData}"
        )
        return ret

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.__dict__ == other.__dict__

    def load_csv(self, csv_file):
        file = open(csv_file)
        reader = csv.reader(file)
        self.features_names = next(reader)
        self.nb_features = len(self.features_names)
        for row in reader:
            if len(row) != self.nb_features:
                raise ValueError(strings.row_wrong_format)
            self.values.append(row)

    def describe(self):
        pass
