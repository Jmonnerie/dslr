import pytest
import pandas as pd
from sample.describe import describe


class TestDescribeClass:
    def test_describe_has_good_output(self):
        df = pd.read_csv("data/dataset_train.csv")
        expected_output = str(df.describe())
        output = describe("data/dataset_train.csv")
        assert expected_output == output
