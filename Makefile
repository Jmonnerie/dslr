# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/28 03:11:41 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/24 21:57:57 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

.PHONY: all install describe histogram scatter_plot logreg_train logreg_predict tests test_one

# ############################################################################ #
#                                VARS                                          #
# ############################################################################ #

QUIET = @
PYTHON = python
PIP = pip
TESTER = pytest

# ############################################################################ #
#                           PROJECT MANAGEMENT                                 #
# ############################################################################ #

venv:
	$(QUIET)pip3 install virtualenv
	$(QUIET)virtualenv venv
	$(QUIET)source venv/bin/activate

install: venv
	$(QUIET)$(PIP) install -r requirements.txt

freeze:
	$(QUIET)$(PIP) freeze > requirements.txt

lint:
	$(QUIET)pre-commit run --all-files

# ############################################################################ #
#                           	PROGRAMS                                       #
# ############################################################################ #

describe:
	$(PYTHON) sample/describe.py $(filter-out $@,$(MAKECMDGOALS))

histogram:
	$(QUIET)$(PYTHON) sample/histogram.py $(filter-out $@,$(MAKECMDGOALS))

scatter_plot:
	$(QUIET)$(PYTHON) sample/scatter_plot.py $(filter-out $@,$(MAKECMDGOALS))

pair_plot:
	$(QUIET)$(PYTHON) sample/pair_plot.py $(filter-out $@,$(MAKECMDGOALS))

logreg_train:
	$(QUIET)$(PYTHON) sample/logreg_train.py dataset_train.csv

logreg_predict:
	$(QUIET)$(PYTHON) sample/logreg_predict.py dataset_test.csv


# ############################################################################ #
#                                  TESTS                                       #
# ############################################################################ #

tests:
	$(QUIET)$(TESTER)

test_one:
	$(QUIET)$(PYTHON) $(filter-out $@,$(MAKECMDGOALS))

coverage:
	$(QUIET)coverage run -m $(TESTER)

%:
	$(QUIET):
